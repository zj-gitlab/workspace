package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

func updateLocalRepo(p Project) error {
	localPath, err := LocalPath(p)
	if err != nil {
		return err
	}

	fi, err := os.Stat(localPath)
	if os.IsNotExist(err) || !fi.IsDir() {
		return gitClone(p.GetSSHURL(), localPath)
	}

	return gitFetch(localPath)
}

// gitFetch depends on an origin remote to exist.
func gitFetch(path string) error {
	log.Printf("fetching origin on %s", path)

	// TODO Doesn't support bare repos
	cmd := exec.Command("git", []string{fmt.Sprintf("--git-dir=%s/.git", path), "fetch", "origin"}...)

	return cmd.Run()
}

func gitClone(sshURL, path string) error {
	log.Printf("cloning %s to %s", sshURL, path)

	if err := os.MkdirAll(path, 0755); err != nil {
		log.Printf("Failure creating dir")
		log.Printf(path)
		return err
	}

	cmd := exec.Command("git", []string{"clone", sshURL, path}...)
	return cmd.Run()
}

func LocalPath(p Project) (string, error) {
	attributes := []string{cfg.RootPath}

	for _, attr := range cfg.Attributes {
		resolved, err := resolveAttribute(p, attr)
		if err != nil {
			return "", err
		}

		attributes = append(attributes, resolved)
	}

	return filepath.Join(attributes...), nil
}
