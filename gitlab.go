package main

import "github.com/xanzy/go-gitlab"

type GitLab struct {
	project *gitlab.Project
}

func gitlabClient() *gitlab.Client {
	return gitlab.NewClient(nil, cfg.GitLab.Token)
}

// GetName returns a normalized path of the URL GitLab uses. This behaviour
// was choosen to mimic the setup of the Go workspace.
func (g *GitLab) GetName() string {
	return g.project.Path
}

// GetNamespace returns a normalized path of the URL GitLab uses. This
// behaviour was choosen to mimic the setup of the Go workspace.
func (g *GitLab) GetNamespace() string {
	return g.project.Namespace.Path
}

func (g *GitLab) GetSSHURL() string {
	return g.project.SSHURLToRepo
}

func (g *GitLab) GetWebURL() string {
	return g.project.WebURL
}

func (g *GitLab) GetProvider() string {
	return "gitlab.com"
}
