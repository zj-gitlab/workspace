package main

import (
	"log"
	"os"
	"path/filepath"
	"sync"
)

var wg = sync.WaitGroup{}

func main() {
	if err := LoadConfig(configFilePath()); err != nil {
		log.Fatal(err)
	}

	// Add one in the waitgroup, call done on it when all providers have
	//listed their projects
	wg.Add(1)

	pChan := make(chan Project, 100)

	for i := 0; i < 4; i++ {
		go func() { updateProjectWorker(pChan) }()
	}

	go func() {
		for _, pr := range Providers() {
			projects, err := pr.ListProjects()

			if err != nil {
				log.Printf("error listing projects: %v", err)
			}

			for _, p := range projects {
				wg.Add(1)
				pChan <- p
			}
		}
		wg.Done()
	}()

	wg.Wait()
	close(pChan)
}

func updateProjectWorker(pChan chan Project) {
	for p := range pChan {
		if err := updateLocalRepo(p); err != nil {
			log.Printf("error: %s", err)
		}
		wg.Done()
	}
}

func configFilePath() string {
	homeDir := os.Getenv("HOME")

	return filepath.Join(homeDir, ".config", "workspace", "config.toml")
}
