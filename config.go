package main

import (
	"fmt"
	"os"

	"github.com/BurntSushi/toml"
)

var cfg Config

type Config struct {
	GitLab GitLabOpt `toml:"gitlab"`
	GitHub GitHubOpt `toml:"github"`

	// The assumption is that there's a container directory; ~/dev, or ~/src. Than
	// based on attributes the rest of the path for the repository is build.
	RootPath   string   `toml:"root_path"`
	Attributes []string `toml:"attributes"`
}

type GitLabOpt struct {
	Token string `toml:"token"`
}

// TODO GH support
type GitHubOpt struct {
	Token string `toml:"token"`
}

func LoadConfig(path string) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	if _, err := toml.DecodeReader(f, &cfg); err != nil {
		return nil
	}

	return cfg.validate()
}

func Providers() []Provider {
	var ps []Provider
	if cfg.GitLab.Token != "" {
		ps = append(ps, &GitLabCom{cfg.GitLab.Token})
	}

	return ps
}

func (c *Config) validate() error {
	if cfg.RootPath == "" {
		fmt.Errorf("no root_path configured, unable to determine the local storage location")
	}

	if len(cfg.Attributes) < 1 {
		return fmt.Errorf("no attributes set to determine the local storage location")
	}

	if len(Providers()) == 0 {
		return fmt.Errorf("no git provider configured, configure at least one of GitLab or GitHub")
	}

	return nil
}
