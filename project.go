package main

import (
	"fmt"
)

type Project interface {
	GetName() string
	GetNamespace() string
	GetSSHURL() string

	GetProvider() string
}

func resolveAttribute(p Project, attr string) (string, error) {
	switch attr {
	case "name":
		return p.GetName(), nil
	case "namespace":
		return p.GetNamespace(), nil
	case "provider":
		return p.GetProvider(), nil
	default:
		return "", fmt.Errorf("unresolvable attribute: %s", attr)
	}
}
