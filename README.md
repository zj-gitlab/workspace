## Workspace

For each GitLab.com project that you awarded a star, or you own, workspace will
clone it to your local workspace.

Limitations:
- [ ] SSH clone only
- [ ] GitLab only
- [ ] Handle forks better, clone using upstream attributes, set origin/upstream
      remotes
