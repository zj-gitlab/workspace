package main

import "github.com/xanzy/go-gitlab"

type Provider interface {
	ListProjects() ([]Project, error)
}

type GitLabCom struct {
	token string
}

func (g *GitLabCom) ListProjects() ([]Project, error) {
	var projects []Project
	opts := []*gitlab.ListProjectsOptions{
		&gitlab.ListProjectsOptions{
			Owned:   gitlab.Bool(false),
			Starred: gitlab.Bool(true),
			ListOptions: gitlab.ListOptions{
				PerPage: 50,
				Page:    1,
			},
		},
		&gitlab.ListProjectsOptions{
			Owned:   gitlab.Bool(true),
			Starred: gitlab.Bool(false),
			ListOptions: gitlab.ListOptions{
				PerPage: 50,
				Page:    1,
			},
		},
	}

	for _, opt := range opts {
		ps, _, err := gitlabClient().Projects.ListProjects(opt, nil)
		if err != nil {
			return nil, err
		}

		for i := range ps {
			projects = append(projects, &GitLab{ps[i]})
		}
	}

	return projects, nil
}
